<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html >
    <head>
        <title> Expelliarmus Tech..</title>
        <link rel="stylesheet" type="text/css" href="default.css"/>
        <h1 style="text-decoration: none;color:black; font-size: 50px;"><center> Batowski</center></h1>
        <script>
        
function validform1() {
    var p = document.forms["myformr"]["emailid"].value;
                    if(p == "") {
                        alert("Enter the emailid");
                        return false;
                    }    
    var q = document.forms["myformr"]["password"].value;
                    if(q == "") {
                        alert("Enter the password");
                        return false;
                    }    
    var r = document.forms["myformr"]["confirm_password"].value;
                    if(r == "") {
                        alert("Enter the confirm_password");
                        return false;
                    }   
    var q = document.forms["myformr"]["password"].value;
    var r = document.forms["myformr"]["confirm_password"].value;
                    if(q != r){
                        alert("enter the same password");
                        return false;
                    }
    var x = document.forms["myformr"]["FirstName"].value;
                if(x == "") {
                    alert("Enter the firstname");
                    return false;
                    }
    var y = document.forms["myformr"]["LastName"].value;
                    if(y == "") {
                        alert("Enter the lastname");
                        return false;
                    }    
    var za = document.forms["myformr"]["birthday"].value;
                    if(za == "") {
                        alert("Select the birth day");
                        return false;
                    }    
    var n = document.forms["myformr"]["gender"].value;
                    if(n == "") {
                        alert("Enter the gender");
                        return false;
                    }   
    var m = document.forms["myformr"]["Phone"].value;
                    if(m == "") {
                        alert("Enter the phonenumber");
                        return false;
                    }     
    var m = document.forms["myformr"]["City"].value;
                    if(m == "") {
                        alert("Enter the city");
                        return false;
                    } 
}
  
        </script>
    </head>
    <body>    
        <form name ="myformr" action="connectdb_register.jsp" class="register" method="post" onsubmit="return validform1()">
            <div class="h2"><br>
            <h2>Registration</h2>
            <fieldset class="row1">
                <legend>Account Details
                </legend>
                <p>
                    <label>Email*
                    </label>
                    <input type="text" name="emailid" placeholder="Enter your email id"/>
                    
                </p>
                <p>
                    <label>Password*
                    </label>
                    <input type="password" name="password" placeholder="Enter strong password" minlength="8"/>
                    <label>Repeat Password*
                    </label>
                    <input type="password" name="confirm_password" placeholder="confirm password" minlength="8"/>
                    
                </p>
            </fieldset>
            <fieldset class="row2">
                <legend>Personal Details
                </legend>
                <p>
                    <label>First Name *
                    </label>
                    <input type="text" name="FirstName" placeholder="Enter your first name here" class="long"/>
                </p>
                <p>
                    <label>Last Name *
                    </label>
                    <input type="text" name="LastName" placeholder="Enter your last name here" class="long"/>
                </p>
                <p>
                    <label>Phone *
                    </label>
                    <input type="text" name="Phone" placeholder="Enter your phone number here" maxlength="10"/>
                </p>
                <p>
                    <label>City *
                    </label>
                    <input type="text" name="City" placeholder="Enter city name here" class="long"/>
                </p>
                <p>
                    <label>Country *
                    </label>
                    <select name="Country">
                        <option value="2">India 
                        </option>
                        <option value="1">United States of America 
                        </option>
                        <option value="3">South Korea 
                        </option>
                        <option value="4">Sudan
                        </option>
                        <option value="5">Australia
                        </option>
                        <option value="6">Great Britain
                        </option>
                        <option value="7">Argentina
                        </option>
                        <option value="8">Portugal
                        </option>
                        <option value="9">Japan
                        </option>
                        <option value="10">Thailand
                        </option>
                    </select >
                </p>
            </fieldset>
            <fieldset class="row3">
                <legend>Further Information
                </legend>
                <p>
                    <label>Gender *</label>
                    <input type="radio" name = "gender" value="Male" />
                    <label class="gender">Male</label>
                    <input type="radio" name= "gender" value="Female"/>
                    <label class="gender">Female</label>
                </select>
                </p>
                <p>
                    <label>Date of Birth</label>
                    <input type="date" class="" name="birthday" placeholder="Enter your date of birth">
                </p>
                <div class="infobox"><h4>Note</h4>
                    <p>Fields marked with '*' are mandatory </p>
                </div>
            </fieldset>
            <fieldset class="row4">
                <legend>Terms and Mailing
                </legend>
                <p class="agreement">
                    <input type="checkbox" required/>
                    <label>*  I accept the <a href="#">Terms and Conditions</a></label>
                </p>
            </fieldset>
            <div><button class="button">Register &raquo;</button></div>
        </form>
    </body>
</html>





